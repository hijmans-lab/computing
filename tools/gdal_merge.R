# Take multiple files and combine into multiband raster

# gdal_merge.py -O basename.tif -separate -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=2 -co ZLEVEL=9

#1st arg, directory of images
dpath <- '~/biogeo/uav/flights/WDA-2016-11-03/MicasenseRedEdge/0017SET/000'
rfiles <- list.files(dpath, pattern="tif", full.names = TRUE )
ofiles <- unique(substring(basename(rfiles),1,8))


gdalmerge <- function(ofile, rfiles){
  # Given a unique file, grab the band and combine
  library(gdalUtils) #gdalmerge isn't in it
  bands <- grep(ofile, rfiles, value=TRUE)
  oname <- paste0('-o ',file.path(dirname(bands[1]),paste0(ofile,'.tif')))
  param <- c('-separate','-of GTiff','-co COMPRESS=DEFLATE','-co PREDICTOR=2','-co ZLEVEL=9')
  #inputs <- paste(bands, collapse=" ")
  system2('gdal_merge.py', args=c(bands,param,oname))
} 

stackmethod <- function(ofile,rfiles){
  #Compare against using stack with R
  library(raster)
  bands <- grep(ofiles, rfiles, value=TRUE)
  param <- c('COMPRESS=DEFLATE','PREDICTOR=2','ZLEVEL=9')
  oname <- paste0(ofile,'.tif')
  rstacked <- stack(bands)
  writeRaster(rstacked, filename = oname,format='Gtiff',options=param)
}

#TODO, include copying of the GPS Exif data