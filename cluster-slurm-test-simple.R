#! /usr/bin/Rscript
# Script to test computing cluster methods, this is the SLURM variant
# Takes a directory of files, each containing 10,000 test scores, calculates summary statstics and a histogram saving both out to file for each of the original files.
# Alex Mandel 2014

#This is designed for the computer cluster, call it with the clusterR.sh
# Usage: sbatch --array=1-10 --job-name=test1 --partition=serial --mail-user=<you>@ucdavis.edu /home/<username>/cluster-test/clusterR.sh /home/<username>/cluster-test/cluster-slurm-test-simple.r

#TODO add process timer and hostname or comput node id to captured output
start <- proc.time()
# Read in file name from stdin or arg
inputn <- Sys.getenv("SLURM_ARRAY_TASK_ID")
print(inputn)


# load file indicated by looking up a file index for the job, 
# keep jobs in separate directories or pass the index file as a command line arg?
dir <- Sys.getenv("SLURM_SUBMIT_DIR")
# Set the working directory to the directory the job was submitted from, this will be the input and output location
# TODO how would this work if the job and files are all hdfs in hadoop?
#setwd(dir)
print(dir)


#End timer
end <- proc.time()
timer <- end-start
#capture.output(timer,file=outfile,append=TRUE)
print(timer)
