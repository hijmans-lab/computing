#!/bin/bash -l
#SBATCH -J MyJob

module load spack/R/4.1.1
#module load R/4.1.0
#module load R
hostname -f
echo $SLURM_ARRAY_TASK_ID

#Use Rscript when you want debug info and print() to work
echo " Rscript --vanilla ${1} ${2} "
Rscript --vanilla ${1} ${2} ${3}

# Rscript is prefered over R CMD Batch
#echo "R CMD BATCH --no-save --no-restore ${1} ${2}"
#R CMD BATCH --no-save --no-restore ${1} ${2}

