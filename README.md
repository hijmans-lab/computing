# README #

This Project is intended to serve as documentation and examples for how to use compute resources in the Hijmans lab group.

### Get Started ###
You probably want to start by looking at the [Wiki](https://bitbucket.org/hijmans-lab/computing/wiki/) associated with this code.


### Need help? ###

Talk to Alex