#!/bin/bash -l
#SBATCH -J MyJob

module load python3
hostname -f
echo $SLURM_ARRAY_TASK_ID

# Calling python3, currently 3.6.x on FARM
echo " python3 ${1} ${2} "
python3 ${1} ${2} ${3}


