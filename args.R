#!/usr/bin/Rscript
# This script shows you how R interprets command line arguements
# Use it to debug how many args you have, and what order they are in
# TODO: add example showing how named args work

#Usage: Rscript --vanilla args.r --args Hello World
args <- commandArgs(trailingOnly=TRUE)
print(args)

#Named args? asValue=TRUE will interpret 
# --foo something
# AS
# -foo=something

#TODO isn't working named args seems to have been removed
# More info http://www.inside-r.org/packages/cran/R.utils/docs/commandArgs
#Usage: Rscript --vanilla args.r --args --one Hello --two World
#args2 <- commandArgs(trailingOnly=TRUE)
#print(args2)

#Test if arg was passed, if not print something else
#ifelse((length(args) > 0),print(args),print(FALSE))
if (length(args) > 0){
    print(args)
} else
print(FALSE)

print(length(args))

#args <- commandArgs(TRUE)
if (length(args) == 1){
    vars <- args[1]    
} else if (length(args) == 2){
    vars <- args[1]
    recode <- args[2]
} else {
    vars <- 'hw8'
    recode <- "KR"
}
    
#print(vars)
print(recode)