#!/bin/sh

#Take 2 args, R script and Control file
module load R

Rfile=$1 

if [ $2 ] ; then 
   #only if a ctl is passed
    Ctl=$2
    #Add one to the count of lines in control file, since R starts counting at 1
    len=$(( `wc -l < $Ctl`+1 ))
    #Path to exectuble? Currently run it all from same folder, you can pass a relative path to the Rfile and Ctl
    ./clusterR.sh ${Rfile} ${Ctl}
    echo "Running script with control file"  
    sbatch --array=1-${len} --job-name=test1 --partition=serial --mail-user=aimandel@ucdavis.edu clusterR.sh ${Rfile} ${Ctl}
else
    echo "Running script no control file" 
    sbatch --job-name=test1 --partition=serial --mail-user=aimandel@ucdavis.edu clusterR.sh ${Rfile}
fi 



