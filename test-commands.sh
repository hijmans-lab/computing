#Simple single job
sbatch -p serial test.sh

#Running a single job within an array
sbatch --array=2 --job-name=test1 --partition=serial --mail-user=aimandel@ucdavis.edu /home/vulpes/cluster-test/clusterR.sh /home/vulpes/cluster-test/cluster-slurm-test-simple.r

#Running a job on 10 items in an array
vulpes@farm:~/cluster-test/example$ sbatch --array=1-10 --job-name=test10 --partition=serial --mail-user=aimandel@ucdavis.edu /home/vulpes/cluster-test/clusterR.sh /home/vulpes/cluster-test/cluster-slurm-test.r

#Checking the queue
squeue -u $(whoami)

#Running a DHS interpolation
sbatch -p serial /home/vulpes/computing/clusterR.sh /home/vulpes/dhs/DHS/preprocess/3_interpolate.R v414i