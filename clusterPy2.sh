#!/bin/bash -l
#SBATCH -J MyJob

module load python
hostname -f
echo $SLURM_ARRAY_TASK_ID

# Calling python, currently 2.7.x on FARM
echo " python ${1} ${2} "
python ${1} ${2} ${3}


