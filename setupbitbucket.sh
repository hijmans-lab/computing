#!/bin/sh
# This script sets up the ssh deployment key so you have read only git access to repos
cp ssh/* ~/.ssh/
chmod 644 ~/.ssh/config
chmod 644 ~/.ssh/farm_biogeo.pub
chmod 600 ~/.ssh/farm_biogeo
