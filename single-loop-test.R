#!/usr/bin/Rscript
#Script to test computing cluster methods, this is the loop variant on a single machine
#Takes a directory of files, each containing 10,000 test scores, calculates summary statstics and a histogram saving both out to file for each of the original files.
#Alex Mandel 2014

#Usage Example, 1st arg is the directory of data (csv files)
#Rscript single-loop-test.R example

#PsuedoCode
start <- proc.time()

#Get dir from arguments
dir <- commandArgs(TRUE)[1]
# Set the working directory to the directory the job was submitted from, this will be the input and output location
# TODO how would this work if the job and files are all hdfs in hadoop?
setwd(dir)
#print(getwd())

#Read in file list from the dir
input <- list.files(pattern="*.csv", full.names=FALSE) 
#print(input)

#loop over the file list or use lapply? depends on how much memory to consume at once?
# http://stackoverflow.com/a/9565095

for(i in 1:length(input)) {
    ##This part is identical to non-slurm variant
    #load file indicated
    d1 <- read.table(input[i], row.names=1,sep=",")
    
    rootfile <- strsplit(input[i],"\\.")[[1]][1]
    outfile <- paste(rootfile,".summary",sep="")
    
    #calculate summary stats on data
    capture.output(summary(d1),file=outfile)
    #capture summary stats
    #create a plot of the data to png or pdf
    outgraph <- png(file=paste(rootfile,".png",sep=""),width=400,height=300)
    hist(d1$V2)
    dev.off()
}

#End timer
end <- proc.time()
timer <- end-start
capture.output(timer,file=outfile,append=TRUE)